#include <cstdio>
#include "sum.h"

int main(){
	int n;

	// input : n 
	scanf("%d",&n);
	int s = sum(n);
	printf("sum = %d\n", s);

	// main 
	// -> if the line "return 0;" doesn't exist, 
	//    the complier add the line "return 0;"
 	return 0;
}


